import json
import tweepy
import sys
import jsonpickle
import os
import time

# Replace the API_KEY and API_SECRET with your application's key and secret.

API_KEY = "rZxIf59T0bcPC5C8AlZt0k8cY"
API_SECRET = "AS0hBqe8xvNLtxelEv1PAN69Uz2xdIfUn4mKhGmmyPUIAFI7Du"
auth = tweepy.AppAuthHandler(API_KEY, API_SECRET)

fName = '/mnt/tweetData/userTimelineTweets.txt' # We'll store the tweets in a text file.
userIdFile = 'user_sample.txt'
timelineLog = open('/mnt/tweetData/timelineLog.txt','a')

api = tweepy.API(auth, wait_on_rate_limit=True,
                                   wait_on_rate_limit_notify=True)

if (not api):
        print ("Can't Authenticate")
        sys.exit(-1)


def userSearch(userID, f):
        #searchQuery = '#someHashtag'  # this is what we're searching for
        maxTweets = 400 # Some arbitrary large number
        tweetsPerQry = 200  # this is the max the API permits

        # If results from a specific ID onwards are reqd, set since_id to that ID.
        # else default to no lower limit, go as far back as API allows
        sinceId = None

        # If results only below a specific ID are, set max_id to that ID.
        # else default to no upper limit, start from the most recent tweet matching the search query.
        max_id = -1L

        tweetCount = 0
        timelineLog.write("Downloading max {0} tweets for {1}".format(maxTweets, userID)+'\n')

        while tweetCount < maxTweets:
                try:
                        if (max_id <= 0):
                                if (not sinceId):
                                        new_tweets = api.user_timeline(user_id = userID, count=tweetsPerQry)
                                else:
                                        new_tweets = api.user_timeline(user_id = userID, count=tweetsPerQry,
                                                                                        since_id=sinceId)
                        else:
                                if (not sinceId):
                                        new_tweets = api.user_timeline(user_id = userID, count=tweetsPerQry,
                                                                                        max_id=str(max_id - 1))
                                else:
                                        new_tweets = api.user_timeline(user_id = userID, count=tweetsPerQry,
                                                                                        max_id=str(max_id - 1),
                                                                                        since_id=sinceId)
                        if not new_tweets:
                                timelineLog.write("No more tweets found for {0}".format(userID)+'\n')
                                break
                        for tweet in new_tweets:
                                f.write(jsonpickle.encode(tweet._json, unpicklable=False) +
                                                '\n')
                        tweetCount += len(new_tweets)
                        timelineLog.write(("Downloaded {0} tweets for {1}".format(tweetCount, userID))+'\n')
                        max_id = new_tweets[-1].id
			#time.sleep(4)
                except tweepy.TweepError as e:
                        # Just exit if any error
                        timelineLog.write("some error : " + str(e)+'\n')
                        break

	timelineLog.write(("Downloaded {0} tweets for {2}, Saved to {1}".format(tweetCount, fName, userID))+'\n')
        return tweetCount

################revoking method#########

total_count = 0
with open(fName, 'w') as f, open(userIdFile,'r') as fUser:
        start_time = time.time()
        print (start_time)
        for i, line in enumerate(fUser):
                if i > 4155 and i <= 8279:
                        chopped_line = line[:-2]
                        #print chopped_line
                        j = json.loads(chopped_line)
                        userID = j['key']
                        total_count += userSearch(userID, f)
                        timelineLog.write("Downloaded {0} tweets in total, Saved to {1}".format(total_count, fName)+'\n')
timelineLog.write("time spent here were -- %s seconds --" % (time.time() - start_time)+'\n')

