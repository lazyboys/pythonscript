import tweepy
import sys
import jsonpickle
import os

# Replace the API_KEY and API_SECRET with your application's key and secret.

API_KEY = "rZxIf59T0bcPC5C8AlZt0k8cY"
API_SECRET = "AS0hBqe8xvNLtxelEv1PAN69Uz2xdIfUn4mKhGmmyPUIAFI7Du"
auth = tweepy.AppAuthHandler(API_KEY, API_SECRET)

api = tweepy.API(auth, wait_on_rate_limit=True,
                   wait_on_rate_limit_notify=True)

if (not api):
    print ("Can't Authenticate")
    sys.exit(-1)



#searchQuery = '#someHashtag'  # this is what we're searching for
maxTweets = 10000000 # Some arbitrary large number
tweetsPerQry = 100  # this is the max the API permits
#fName = 'tweets.txt' # We'll store the tweets in a text file.

fName = 'northEastLondonTweetSearch.txt' # We'll store the tweets in a text file.
bfName = open('/mnt/tweetData/northEastLondonTweetSearch.txt','a')
searchLog = open('/mnt/tweetData/searchLog.txt','a')


# If results from a specific ID onwards are reqd, set since_id to that ID.
# else default to no lower limit, go as far back as API allows
sinceId = None

# If results only below a specific ID are, set max_id to that ID.
# else default to no upper limit, start from the most recent tweet matching the search query.
max_id = -1L

tweetCount = 0
print("Downloading max {0} tweets".format(maxTweets))
with open(fName, 'w') as f:
    while tweetCount < maxTweets:
        try:
            if (max_id <= 0):
                if (not sinceId):
                    new_tweets = api.search(geocode='51.5155,-0.0922,50km', count=tweetsPerQry)
                else:
                    new_tweets = api.search(geocode='51.5155,-0.0922,50km', count=tweetsPerQry,
                                            since_id=sinceId)
            else:
                if (not sinceId):
                    new_tweets = api.search(geocode='51.5155,-0.0922,50km', count=tweetsPerQry,
                                            max_id=str(max_id - 1))
                else:
                    new_tweets = api.search(geocode='51.5155,-0.0922,50km', count=tweetsPerQry,
                                            max_id=str(max_id - 1),
                                            since_id=sinceId)
            if not new_tweets:
                print("No more tweets found")
                break
            for tweet in new_tweets:
                f.write(jsonpickle.encode(tweet._json, unpicklable=False) + '\n')
                bfName.write(jsonpickle.encode(tweet._json, unpicklable=False) + '\n')

            tweetCount += len(new_tweets)
            #print("Downloaded {0} tweets".format(tweetCount))
            searchLog.write(str(tweetCount)+"\n")
            max_id = new_tweets[-1].id
            if tweetCount==500000:
                fName.close()
                bfName.close()
                searchLog.close()
                sys.exit()

        except tweepy.TweepError as e:
            # Just exit if any error
            print("some error : " + str(e))
            #break
            continue
            sys.exc_clear()

#print ("Downloaded {0} tweets, Saved to {1}".format(tweetCount, fName))

