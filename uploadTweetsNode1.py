import json
from vaderSentiment.vaderSentiment import sentiment as vaderSentiment
from couchdb import Server

server = Server('http://127.0.0.1:5984/')

db = server['tweetDBNode1']

tweetFile = open('/mnt/tweetData/southEastLondonTweetSearch.txt', 'rb')
dbStoreLog = open('/mnt/tweetData/docIdRevLog.txt', 'w')

successCount = 0
failCount = 0
duplicateCount = 0

for row in tweetFile:
	try:
        	twitterMsg = json.loads(row)
	except:
		failCount = failCount + 1
		continue

        tweet = str(twitterMsg['text'].encode('utf-8'))
        ID = twitterMsg['id_str']

        try:
                analyzedTweet = vaderSentiment(tweet)

        except:
                failCount = failCount + 1
                continue

        sentiment = {}
        docID = {}
        sentiment['sentiment'] = analyzedTweet
        docID['_id'] = ID
        jsonSentiment = json.dumps(sentiment)
        jsonID = json.dumps(docID)
        tweetSentiment = json.loads(jsonSentiment)
        tweetID = json.loads(jsonID)
        tweetWithSentiment = dict(tweetID.items() + twitterMsg.items() + tweetSentiment.items())
        doc = tweetWithSentiment

	try:
                doc_id, doc_rev = db.save(doc)
                dbStoreLog.write("ID: ")
                dbStoreLog.write(doc_id)
                dbStoreLog.write(" Rev: ")
                dbStoreLog.write(doc_rev)
                dbStoreLog.write("\n")
                successCount = successCount + 1
        except:
                duplicateCount = duplicateCount + 1
                continue



print "\nTotal tweet stored: ", successCount
print "\nTotal duplicate found: ", duplicateCount
print "\nStoring failed: ", failCount


